﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartController : MonoBehaviour
{
    [SerializeField] private GameObject floorHeart;

    [SerializeField] private string currentPage;
    [SerializeField] private GameObject heartPage;

    [SerializeField] private bool floorDetectionOn;

    private void Awake()
    {
        floorHeart.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        currentPage = GetComponent<pageDetection>().currentPage;

        if (floorDetectionOn)
        {
            // left blank for now
        }

        else
        {
            if (currentPage == heartPage.name)
                floorHeart.SetActive(true);

            else
                floorHeart.SetActive(false);
        }
    }


}
