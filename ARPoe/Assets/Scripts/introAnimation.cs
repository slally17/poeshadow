﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class introAnimation : MonoBehaviour
{
    [SerializeField] private UnityEngine.Video.VideoPlayer videoPlayer;
    [SerializeField] private float offset = 1f;

    private GameObject parent;
    private Vector3 startingPosition;
    private Vector3 finalPosition;
    private float startTime;
    private float animTime;
    
    void Start()
    {
        videoPlayer.loopPointReached += resetBird;

        parent = transform.parent.gameObject;
        startingPosition = parent.transform.position;
        finalPosition = startingPosition + offset * Vector3.right;

        startTime = Time.time;
        animTime = (float)videoPlayer.length;
    }

    private void Update()
    {
        startingPosition = parent.transform.position - 0.306f * parent.transform.right;
        finalPosition = startingPosition + offset * parent.transform.right;

        float t = (Time.time - startTime) / animTime;
        videoPlayer.gameObject.transform.localPosition = Vector3.Lerp(startingPosition, finalPosition, t);
    }

    void resetBird(UnityEngine.Video.VideoPlayer vp)
    {
        vp.gameObject.transform.localPosition = startingPosition;
        startTime = Time.time;
    }
}
