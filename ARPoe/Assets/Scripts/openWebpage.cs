﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openWebpage : MonoBehaviour
{
    [SerializeField] private string url;

    void Start()
    {
        Application.OpenURL(url);
    }
}
