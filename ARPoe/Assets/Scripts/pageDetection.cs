﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using TMPro;

[RequireComponent(typeof(ARTrackedImageManager))]
public class pageDetection : MonoBehaviour
{
    [SerializeField] private Camera sceneCamera;
    [SerializeField] private GameObject[] pagePrefabs;
    [SerializeField] private TextMeshProUGUI text;

    private ARTrackedImageManager manager;
    private Dictionary<string, GameObject> pages = new Dictionary<string, GameObject>();
    private bool locked = false;
    public string currentPage = "";

    [SerializeField] private GameObject lockIcon;


    void Awake()
    {
        manager = GetComponent<ARTrackedImageManager>();

        foreach(GameObject page in pagePrefabs)
        {
            GameObject newPage = Instantiate(page, Vector3.zero, Quaternion.identity);
            newPage.name = page.name;
            newPage.SetActive(false);
            pages.Add(page.name, newPage);
        }

        text.text = "START" + pagePrefabs.Length;

        lockIcon.SetActive(false);
    }

    private void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            locked = !locked;
            lockIcon.SetActive(locked); // adding the lock icon when its locked (might have it backwards)
        }
    }

    void OnEnable() => manager.trackedImagesChanged += OnChanged;

    void OnDisable() => manager.trackedImagesChanged -= OnChanged;

    void OnChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var newImage in eventArgs.added)
        {
            //text.text = newImage.referenceImage.name + " found";
            updatePage(newImage);
        }

        foreach (var updatedImage in eventArgs.updated)
        {
            //text.text = updatedImage.referenceImage.name + " moved";
            updatePage(updatedImage);
        }

        foreach (var removedImage in eventArgs.removed)
        {
            // This foreach never gets called for some reason
            text.text = removedImage.referenceImage.name + " lost";
            pages[removedImage.referenceImage.name].SetActive(false);
        }
    }

    private void updatePage(ARTrackedImage trackedImage)
    {
        if(pagePrefabs != null)
        {
            string name = trackedImage.referenceImage.name;

            if(!locked)
            {
                pages[name].SetActive(true);
                pages[name].transform.position = trackedImage.transform.position;
                pages[name].transform.LookAt(sceneCamera.transform);

                foreach (GameObject page in pages.Values)
                {
                    if (page.name != name)
                    {
                        page.SetActive(false);
                    }
                }
            }
            
            if (name != currentPage)
            {
                currentPage = name;
                locked = false;
                lockIcon.SetActive(false);
            }
        }
    }
}
