﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class helpVideo : MonoBehaviour
{

    [SerializeField] private UnityEngine.Video.VideoPlayer videoPlayer;

    private void Update()
    {
        videoPlayer.loopPointReached += EndReached;
    }

    private void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        vp.gameObject.SetActive(false);
    }

    public void helpClick()
    {
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.Play();
    }
}
