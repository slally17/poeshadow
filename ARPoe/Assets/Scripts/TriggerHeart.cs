﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHeart : MonoBehaviour
{
    public GameObject heart;

    public void HeartSpawn()
    {
        heart.GetComponent<MeshRenderer>().enabled = true;
        Debug.Log("pressed");
    }
}
