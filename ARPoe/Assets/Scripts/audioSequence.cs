﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioSequence : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource1;
    [SerializeField] private AudioSource audioSource2;
    [SerializeField] private AudioClip[] audioArray;
    [SerializeField] private AudioClip audioClip;

    private int curClip = 0;
    private int numClips;
    // Start is called before the first frame update
    void Start()
    {
        numClips = audioArray.Length;
        audioSource1.clip = audioArray[curClip];
        audioSource1.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!audioSource1.isPlaying)
        {
            curClip++;
            if(curClip == numClips)
            {
                curClip = 0;
            }

            audioSource1.clip = audioArray[curClip];
            audioSource1.Play();

            if(curClip == 1)
            {
                audioSource2.clip = audioClip;
                audioSource2.Play();
            }
        }
    }
}
